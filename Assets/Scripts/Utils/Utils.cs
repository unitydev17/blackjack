﻿using UnityEngine;
using System.IO;

public class Utils
{

	private const string FILENAME = "/data.txt";
	private const int DEFAULT_VALUE = 200;

	public static int ReadData ()
	{
		string path = Application.persistentDataPath + FILENAME;

		if (!File.Exists (path)) {
			File.WriteAllText (path, DEFAULT_VALUE.ToString());
		} else {
			int value = int.Parse(File.ReadAllText(path));
			return value;
		}

		return DEFAULT_VALUE;
	}


	public static void SaveData (int value)
	{
		string path = Application.persistentDataPath + FILENAME;
		File.WriteAllText (path, value.ToString());
	}



}
