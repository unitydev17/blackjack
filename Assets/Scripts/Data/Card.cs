﻿
public class Card
{
	private Suit suit;
	public Face face;
	public int value;


	public Card(Face face, Suit suit, int value) {
		this.face = face;
		this.suit = suit;
		this.value = value;
	}


	public string GetFileName() {
		string name;
		int faceNum = (int)face;

		if (faceNum < 9) {
			faceNum += 2;
			name = faceNum.ToString ();
		} else {
			name = (Face)faceNum + "_";
		}

		return "Graphics/cards/" + name + suit.ToString().ToLower ();
	}

}