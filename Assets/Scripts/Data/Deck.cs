﻿using System.Collections.Generic;
using System;

public class Deck
{
	private int numDecks;
	private List<Card> cards;
	private Random rnd;
	private Random shRnd;
	private int shoes;


	public Deck (int numDecks)
	{
		this.numDecks = numDecks;
		rnd = new Random ();
		shRnd = new Random ();
		NewDeck ();
	}


	void NewDeck ()
	{
		Initialize (numDecks);

		// shuffle twice for more straight distribution 
		Shuffle (2);
		shoes = shRnd.Next ((int)(0.75f * cards.Count), cards.Count);
	}


	public List<Card> GetPair() {
		List<Card> pair = new List<Card> ();
		pair.Add (GetCard ());
		pair.Add(GetCard());
		return pair;
	}


	public Card GetCard() {

		/*
		if (true) {
			return new Card (Face.Ace, Suit.Club, 11);
		} // test purpose only
		*/


		if (cards.Count == shoes) {
			NewDeck ();
		}

		Card card = cards [0];
		cards.RemoveAt (0);
		return card;
	}


	private void Shuffle(int num) {
		for (int i = 0; i < num; i++) {
			Shuffle ();
		}
	}


	/**
	 * Shuffle is performed with Fisher-Yets-Durstenfeld-Knut algorithm. 
	 * Advantages are speed and shuffle "on a place".
	 **/
	private void Shuffle() {
		int lastIdx = cards.Count - 1;
		int swapIdx;
		for (int i = lastIdx; i > 0; i--) {
			swapIdx = rnd.Next (i);
			Card swap = cards [i];
			cards [i] = cards [swapIdx];
			cards [swapIdx] = swap;
		}
	}


	private int GetValue (int idx, Face face)
	{
		if (Face.Ace == face) {
			return 11;	
		}

		int value = idx + 2;
		if (value > 10) {
			value = 10;
		}
		return value;
	}


	private void Initialize (int numDecks)
	{
		cards = new List<Card> ();
		Card card;
		int value;
		Face face;

		for (int k = 0; k < numDecks; k++) {
			for (int i = 0; i < 13; i++) {
				for (int j = 0; j < 4; j++) {
					face = (Face)i;
					value = GetValue (i, face);
					card = new Card (face, (Suit)j, value);
					cards.Add (card);
				}
			}
		}

	}


}