﻿using System.Collections.Generic;
using System.Linq;
using System;


public class Hand
{
	public LinkedList<Card> cards;
	public int bet;
	public int betInsurance;
	public bool isFinalized;
	public bool isAceSplitted;
	public bool isSplitted;

	public Hand ()
	{
		cards = new LinkedList<Card> ();
	}

	public Hand (Card card1, Card card2)
	{
		cards = new LinkedList<Card> ();
		cards.AddLast (card1);
		cards.AddLast (card2);
	}

	public Hand (List<Card> pair)
	{
		cards = new LinkedList<Card> ();
		cards.AddLast (pair [0]);
		cards.AddLast (pair [1]);
	}

	public int GetValue ()
	{
		if (cards.Count > 1) {
			return cards.Select (card => card.value).Aggregate ((a, b) => a + b);
		}
		return cards.First.Value.value;
	}

	public int GetValue (bool isCloseCard)
	{
		if (!isCloseCard) {
			return GetValue ();
		}
		return cards.ElementAt (0).value;
	}

	public void AceNormalize ()
	{
		foreach (Card card in cards) {
			if (Face.Ace == card.face) {
				card.value = 1;
			}
		}
	}

	public void AceDeNormalize ()
	{
		foreach (Card card in cards) {
			if (Face.Ace == card.face) {
				card.value = 11;
			}
		}
	}

	public bool IsBlackJack ()
	{
		return IsBlackJackReached () && cards.Count == 2;
	}


	public bool IsBlackJackReached ()
	{
		return GetValue () == 21;
	}


	public static Hand GetTestHand ()
	{
		Card card1 = new Card (Face.Nine, Suit.Club, 9);
		Card card2 = new Card (Face.Nine, Suit.Diamond, 9);
		var cards = new List<Card> ();
		cards.Add (card1);
		cards.Add (card2);
		Hand hand = new Hand (cards);
		return hand;
	}

	public static Hand GetTestDealerHand ()
	{
		Card card1 = new Card (Face.Ace, Suit.Heart, 11);
		Card card2 = new Card (Face.Ten, Suit.Spade, 10);
		var cards = new List<Card> ();
		cards.Add (card1);
		cards.Add (card2);
		Hand hand = new Hand (cards);
		return hand;
	}

	public bool IsBlackJackPossible ()
	{
		return cards.First.Value.value > 9;
	}

	public bool IsAceOpened ()
	{
		return cards.First.Value.face == Face.Ace;
	}

	public void CopyFrom (Hand hand)
	{
		this.cards = hand.cards;
	}

	public bool IsSplitableCards ()
	{
		Card first = cards.First.Value;
		Card second = cards.First.Next.Value;
		return cards.Count == 2 && first.value == second.value;
	}

	public Hand Split (List<Card> pair)
	{
		AceDeNormalize ();

		Card first = cards.First.Value;
		Card second = cards.First.Next.Value;
		Hand hand = new Hand (second, pair [0]);
		cards.First.Next.Value = pair [1];
		hand.bet = this.bet;

		if (Face.Ace == first.face) {
			this.isAceSplitted = true;
			hand.isAceSplitted = true;
		}

		this.isSplitted = true;
		hand.isSplitted = true;

		return hand;
	}
}