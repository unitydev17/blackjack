﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

	#region CONSTANTS

	private const string PLAYER_BURST_MSG = "Player Burst";

	private const string DEALER_BURST_MSG = "Dealer Burst";

	private const string PLAYER_WINS_MSG = "Player wins";

	private const string PLAYER_WINS_BLACK_JACK_MSG = "Black Jack. Player wins";

	private const string DEALER_WINS_BLACK_JACK_MSG = "Black Jack. Dealer wins";

	private const string DEALER_WINS_MSG = "Dealer wins";

	private const string PUSH_MSG = "Push";

	private const string NO_BET_MESSAGE = "You need to place a bet!";

	private const string ASK_BLACK_JACK_WIN = "You have black jack.\nDo you agree to get a win 1:1 ?";

	private const string ASK_INSURANCE = "Do you want to buy insurance for half of your bet ?";

	private const string HAND_PREFAB_PATH = "Prefabs/Hand";

	private const string DEALER_HAND_PREFAB_PATH = "Prefabs/DealerHand";

	private const string CARD_PREFAB_PATH = "Prefabs/Card";

	private const string CHIP_PREFAB_PATH = "Prefabs/Chips/Chip";

	private int[] CHIP_VALUES = { 1, 5, 10, 25, 100 };

	#endregion

	#region GAMEOBJECTS

	[SerializeField]
	private int messageDelay = 2;

	[SerializeField]
	private int finalMessageDelay = 4;

	[SerializeField]
	private State state;


	private Deck deck;
	private PlayerModel playerModel;
	private DealerModel dealerModel;
	private Image msgImage;

	public GameObject playerContainer;
	public GameObject splitContainer;
	public GameObject dealerContainer;

	public GameObject splitButton;
	public GameObject doubleDownButton;
	public GameObject hitButton;
	public GameObject standButton;
	public GameObject dealButton;

	public Button chip1Button;
	public Button chip5Button;
	public Button chip10Button;
	public Button chip25Button;
	public Button chip100Button;

	public GameObject betContainer;
	public GameObject betHelper;
	private Text betText;

	public GameObject moneyButton;
	private Text moneyText;

	public GameObject messagePanel;
	public Text messageText;
	public Sprite cardBack;

	public GameObject dialogPanel;
	public Text dialogMessageText;
	public GameObject blockingPanel;
	public GameObject deckCountSelector;


	#endregion

	#region MAIN_CYCLE

	void OnApplicationQuit() {
		Utils.SaveData (playerModel.money);
	}

	void Start ()
	{
		playerModel = new PlayerModel ();
		dealerModel = new DealerModel ();
		state = State.OnSetupBet;
		msgImage = messagePanel.GetComponent<Image> ();
		betText = betHelper.GetComponentInChildren<Text> ();
		moneyText = moneyButton.GetComponentInChildren<Text> ();
		playerModel.money = Utils.ReadData ();
		UpdateMoneyState ();
	}


	void Update ()
	{
		UpdateUI ();
		ProcessStates ();
	}


	private void UpdateUI ()
	{
		if (playerModel.uIModel.HasChanged ()) {
			splitButton.SetActive (playerModel.uIModel.splitActive);
			doubleDownButton.SetActive (playerModel.uIModel.doubleDownActive);
			hitButton.SetActive (playerModel.uIModel.hitActive);
			standButton.SetActive (playerModel.uIModel.standActive);
			dealButton.SetActive (playerModel.uIModel.dealActive);
			messagePanel.SetActive (playerModel.uIModel.messagePanelActive);
			dialogPanel.SetActive (playerModel.uIModel.dialogPanelActive);
			betHelper.SetActive (playerModel.HasBet () || playerModel.uIModel.betHelperActive);

			chip1Button.interactable = playerModel.uIModel.chip1Enable && playerModel.uIModel.chipsAwailable;
			chip5Button.interactable = playerModel.uIModel.chip5Enable && playerModel.uIModel.chipsAwailable;
			chip10Button.interactable = playerModel.uIModel.chip10Enable && playerModel.uIModel.chipsAwailable;
			chip25Button.interactable = playerModel.uIModel.chip25Enable && playerModel.uIModel.chipsAwailable;
			chip100Button.interactable = playerModel.uIModel.chip100Enable && playerModel.uIModel.chipsAwailable;

			blockingPanel.SetActive (playerModel.uIModel.blockInput);
			deckCountSelector.SetActive (playerModel.uIModel.deckSelectorEnable);
			playerModel.uIModel.Refresh ();
		}
	}


	private void ProcessStates ()
	{
		switch (state) {
		case State.OnSetupBet:
			OnSetupBet ();
			break;
		case State.OnStartGame:
			OnStartGame ();
			break;
		case State.PlayerMove:
			OnPlayerMove ();
			break;
		case State.DealerMove:
			OnDealerMove ();
			break;
		case State.CheckWinner:
			OnCheckWinner ();
			break;
		}
	}


	private void OnStartGame ()
	{
		var decksNumber = deckCountSelector.GetComponent<Dropdown> ().value + 1;
		deck = new Deck (decksNumber);

		// init player

		Hand hand = new Hand (deck.GetPair ());
		//Hand hand = Hand.GetTestHand ();
		playerModel.AddHand (hand);
		playerModel.SelectCurrentHand ();

		CheckAceNormalization (playerModel);


		PlacePlayerHand (playerModel.currentHand);
		PlaceSplitContainer ();


		// init dealer
		hand = new Hand (deck.GetPair ());
		//hand = Hand.GetTestDealerHand ();

		dealerModel.SetCurrentHand (hand);
		CheckAceNormalization (dealerModel);
		PlaceDealerHand (hand, true);

		// next state
		state = State.PlayerMove;
		playerModel.uIModel.chipsAwailable = false;
		playerModel.uIModel.deckSelectorEnable = false;
	}


	public void PlaceSplitContainer ()
	{
		ClearSplitContainer ();
		for (int i = 0; i < playerModel.hands.Count; i++) {
			if (playerModel.hands [i] != playerModel.currentHand) {
				PlaceSplittedHand (playerModel.hands [i]);
			}
		}

		state = State.Idle;
	}


	private void ClearSplitContainer ()
	{
		foreach (Transform child in splitContainer.transform) {
			Destroy (child.gameObject);
		}
	}


	private void PlaceSplittedHand (Hand hand)
	{
		GameObject currentHandObj = GetHandInstance (true);
		currentHandObj.transform.SetParent (splitContainer.transform, false);
		GameObject obj = null;
		foreach (Card card in hand.cards) {
			obj = GetCardInstance ();
			var fname = card.GetFileName ();
			var sprite = Resources.Load (fname, typeof(Sprite)) as Sprite;
			var image = obj.GetComponent<Image> ();
			image.sprite = sprite;
			obj.transform.SetParent (currentHandObj.transform, false);
			obj.transform.SetAsLastSibling ();
			if (hand.isFinalized) {
				image.color = Color.gray;
			}
		}

		var valueTextObj = currentHandObj.GetComponentInChildren<Text> ();
		valueTextObj.text = hand.GetValue (false).ToString ();
		valueTextObj.transform.parent.SetAsLastSibling ();

	}


	private void OnPlayerMove ()
	{
		if (playerModel.IsBlackJack ()) {

			if (dealerModel.IsBlackJackPossible ()) {

				if (dealerModel.IsAceOpened ()) {
					AskForBlackJackOneToOneWin ();
				} else {
					state = State.DealerMove;
					return;
				}

			} else {
				StartCoroutine (ShowFinalMessage (PLAYER_WINS_BLACK_JACK_MSG));
				playerModel.Win ();
				UpdateMoneyState ();
			}

		} else {

			if (IsInsuranceAvailable () && !playerModel.IsAskedForInsurance ()) {
				AskForInsurance ();
			
			} else {
				
				// default ui model
				playerModel.uIModel.Reset ();

				if (IsSplitAvailable () && playerModel.IsEnoughMoneyForSplit ()) {
					playerModel.uIModel.splitActive = true;

				} else if (playerModel.IsAceSplitted ()) {
					OnClickStand ();
					return;
				}
					
				playerModel.uIModel.doubleDownActive = true;
				playerModel.uIModel.hitActive = true;
				playerModel.uIModel.standActive = true;
			}
		}

		state = State.Idle;
	}


	public bool IsSplitAvailable ()
	{
		return playerModel.IsSplitableCards ();
	}


	private bool IsInsuranceAvailable ()
	{
		return dealerModel.IsAceOpened () && playerModel.IsEnoughMoneyForInsurance ();
	}


	private void AskForInsurance ()
	{
		dialogMessageText.text = ASK_INSURANCE;
		playerModel.uIModel.Reset ();
		playerModel.uIModel.dialogPanelActive = true;
		playerModel.uIModel.dialogState = UIModel.DialogState.Insurance;
	}


	private void AskForBlackJackOneToOneWin ()
	{
		dialogMessageText.text = ASK_BLACK_JACK_WIN;
		playerModel.uIModel.Reset ();
		playerModel.uIModel.dialogPanelActive = true;
		playerModel.uIModel.dialogState = UIModel.DialogState.BlackJackWinOneToOne;
	}


	private void OnSetupBet ()
	{
		// remove bet
		playerModel.ResetBet ();

		foreach (Transform child in betContainer.transform) {
			Destroy (child.gameObject);
		}

		playerModel.Reset ();
		playerModel.uIModel.Reset ();
		playerModel.UpdateChips ();
		playerModel.uIModel.dealActive = true;
		playerModel.uIModel.chipsAwailable = true;
		playerModel.uIModel.deckSelectorEnable = true;
		playerModel.SetAskedForInsurance (false);
		state = State.Idle;
	}


	private void OnCheckWinner ()
	{

		if (playerModel.HandMoreThan (dealerModel)) {
			playerModel.Win ();
			StartCoroutine (ShowFinalMessage (PLAYER_WINS_MSG));

		} else if (playerModel.HandEquals (dealerModel)) {
			playerModel.Share ();
			StartCoroutine (ShowFinalMessage (PUSH_MSG));

		} else if (playerModel.HandLowerThan (dealerModel)) {
			if (dealerModel.IsBlackJack ()) {
				playerModel.PayInsurance ();
			}
			StartCoroutine (ShowFinalMessage (DEALER_WINS_MSG));
		}

		UpdateMoneyState ();
		state = State.Idle;
	}


	private void UpdateMoneyState ()
	{
		moneyText.text = playerModel.money.ToString ();
	}


	private void UpdateBetState ()
	{
		betText.text = playerModel.GetBets ().ToString ();
	}


	private void OnDealerMove ()
	{

		playerModel.uIModel.Reset ();
		bool isEnough = false;
		do {
			int value = dealerModel.currentHand.GetValue ();

			if (dealerModel.IsSecondCardClosed ()) {
				dealerModel.UnsetSecondCardClosed ();
				RevealClosedCards (dealerModel);

				if (dealerModel.IsBlackJack ()) {
					state = State.CheckWinner;
				} else {
					StartCoroutine (DelayNextState (State.DealerMove));
					state = State.Idle;
				}
				return;
			}

			if (value <= 16) {
				MakeMove (dealerModel, false);
				StartCoroutine (DelayNextState (State.DealerMove));
				state = State.Idle;
				return;

			} else if (value > 21) {
				playerModel.Win ();
				UpdateMoneyState ();
				StartCoroutine (ShowFinalMessage (DEALER_BURST_MSG));
				state = State.Idle;
				return;

			} else {
				isEnough = true;
			}
		} while (!isEnough);

		state = State.CheckWinner;
	}

	#endregion


	#region ON_CLICK_EVENTS

	public void OnClickHome() {
		Application.OpenURL ("http://unity3d.com/");
	}


	public void OnClickSettings() {
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit ();
		#endif
	}


	public void OnClickMoneyBtn ()
	{

		playerModel.money += 200;
		moneyText.text = playerModel.money.ToString ();

		playerModel.UpdateChips ();

		state = State.Idle;
	}


	public void OnClickChip1 ()
	{
		OnClickChip (1);
	}

	public void OnClickChip5 ()
	{
		OnClickChip (5);
	}

	public void OnClickChip10 ()
	{
		OnClickChip (10);
	}

	public void OnClickChip25 ()
	{
		OnClickChip (25);
	}

	public void OnClickChip100 ()
	{
		OnClickChip (100);
	}

	private void OnClickChip (int num)
	{
		if (!playerModel.IsInsured ()) {
			playerModel.AddBet (num);
		}
		PlaceChip (num);
	}


	public void OnClickDeal ()
	{
		if (playerModel.HasNotBet ()) {
			SetCommonMessage (NO_BET_MESSAGE);
			StartCoroutine (DelayNextState (State.OnSetupBet));
			return;
		}
		state = State.OnStartGame;
	}


	public void OnClickSplit ()
	{
		playerModel.BlockInput ();	
		Destroy (playerModel.currentHandObj);

		playerModel.SplitHand (deck.GetPair ());
		PlacePlayerHand (playerModel.currentHand);
		PlaceSplitContainer ();
		PlaceChipsBetNoChange (playerModel.currentHand.bet);
		UpdateBetState ();
		UpdateMoneyState ();

		state = State.PlayerMove;

		playerModel.AllowInput ();
	}


	public void OnClickDoubleDown() {
		playerModel.BlockInput ();

		PlaceChips (playerModel.GetBet(), false);
		playerModel.ApplyDoubleDown ();
		UpdateBetState ();
		UpdateMoneyState ();
	
		DoHit (State.DealerMove);
	}


	public void OnClickHit() {
		DoHit (State.PlayerMove);
	}

	public void DoHit (State nextState)
	{
		playerModel.BlockInput ();
		
		MakeMove (playerModel, true);

		if (playerModel.IsBurst ()) {
			StartCoroutine (ShowFinalMessage (PLAYER_BURST_MSG));

		} else if (IsBlackJackReached ()) {
			state = State.DealerMove;

		} else {
			state = nextState;
		}

		playerModel.AllowInput ();
	}


	public void OnClickOk ()
	{
		if (UIModel.DialogState.BlackJackWinOneToOne == playerModel.uIModel.dialogState) {
			StartCoroutine (ShowFinalMessage (PLAYER_WINS_MSG));
			playerModel.WinOneToOne ();
			UpdateMoneyState ();
			state = State.Idle;

		} else if (UIModel.DialogState.Insurance == playerModel.uIModel.dialogState) {
			playerModel.SetInsurance ();
			PlaceInsuranceChips ();
			state = State.PlayerMove;
		}
	}


	public void OnClickCancel ()
	{
		if (UIModel.DialogState.BlackJackWinOneToOne == playerModel.uIModel.dialogState) {
			state = State.DealerMove;

		} else if (UIModel.DialogState.Insurance == playerModel.uIModel.dialogState) {
			playerModel.uIModel.dialogPanelActive = false;
			playerModel.SetAskedForInsurance (true);
			state = State.PlayerMove;
		}
	}

	public void OnClickStand ()
	{
		playerModel.BlockInput ();
		state = State.DealerMove;
	}

	#endregion

	#region OTHERS


	private bool IsBlackJackReached ()
	{
		return playerModel.currentHand.IsBlackJackReached ();
	}


	private void PlaceInsuranceChips ()
	{
		PlaceChips (playerModel.GetBetInsurance ());
	}


	private void PlaceChips (int num)
	{
		PlaceChips (num, true);
	}

	private void PlaceChipsBetNoChange (int num)
	{
		PlaceChips (num, false);
	}

	private void PlaceChips (int num, bool isChangeBet)
	{
		int qty = 0;
		int numDecr = num;

		for (int i = CHIP_VALUES.Length - 1; (i >= 0) && (qty < num); i--) {
			var value = CHIP_VALUES [i];
			if (numDecr - value >= 0) {
				int count = numDecr / value;
				for (int j = 0; j < count; j++) {
					if (isChangeBet) {
						OnClickChip (value);
					} else {
						PlaceChip (value);
					}
					qty += value;
					numDecr -= value;
				}
			}
		}
	}


	private void PlaceChip (int num)
	{
		UpdateMoneyState ();
		UpdateBetState ();
		playerModel.uIModel.betHelperActive = true;
		playerModel.UpdateChips ();


		GameObject obj = GetChipInstance (num);
		obj.transform.SetParent (betContainer.transform, false);

		Vector3 position = obj.transform.position;
		position.x += Random.value * 50 - 25;
		position.y += Random.value * 50 - 25;
		obj.transform.position = position;

	}

	private void MakeMove (CommonModel model, bool isPlayer)
	{
		Destroy (model.currentHandObj);
		model.AddCardToHand (deck.GetCard ());
		CheckAceNormalization (model);
		if (isPlayer) {
			PlacePlayerHand (model.currentHand);
		} else {
			PlaceDealerHand (model.currentHand, false);
		}
	}

	private void CheckAceNormalization (CommonModel model)
	{
		if (model.IsBurst ()) {
			model.currentHand.AceNormalize ();
		}
	}

	private void RevealClosedCards (CommonModel model)
	{
		Destroy (model.currentHandObj);
		PlaceDealerHand (model.currentHand, false);
	}


	IEnumerator DelayNextState (State nextState)
	{
		yield return new WaitForSeconds (messageDelay);
		playerModel.AllowInput ();
		state = nextState;
	}


	IEnumerator ShowFinalMessage (string message)
	{
		SetCommonMessage (message);
		yield return new WaitForSeconds (finalMessageDelay);

		playerModel.AllowInput ();

		if (playerModel.IsFinalized ()) {
			Destroy (playerModel.currentHandObj);
			Destroy (dealerModel.currentHandObj);
			ClearSplitContainer ();
			playerModel.Reset ();
			dealerModel.Reset ();
			playerModel.uIModel.Reset ();
			state = State.OnSetupBet;
		} else {
			Destroy (playerModel.currentHandObj);
			playerModel.currentHand.isFinalized = true;
			playerModel.SelectCurrentHand ();
			PlacePlayerHand (playerModel.currentHand);
			PlaceSplitContainer ();

			state = State.PlayerMove;
		}

	}


	private void SetMessage (string message, bool isPlayer)
	{
		playerModel.uIModel.Reset ();
		playerModel.uIModel.messagePanelActive = true;
		msgImage.color = isPlayer ? Color.cyan : Color.magenta;
		messageText.text = message;
	}

	private void SetCommonMessage (string message)
	{
		playerModel.uIModel.Reset ();
		playerModel.uIModel.messagePanelActive = true;
		msgImage.color = Color.yellow;
		messageText.text = message;
	}


	public static GameObject GetChipInstance (int value)
	{
		string path = CHIP_PREFAB_PATH + value.ToString ();
		GameObject prefab = (GameObject)Resources.Load (path, typeof(GameObject));
		return GameObject.Instantiate (prefab);
	}


	public static GameObject GetHandInstance (bool isPlayer)
	{
		string path = isPlayer ? HAND_PREFAB_PATH : DEALER_HAND_PREFAB_PATH;
		GameObject prefab = (GameObject)Resources.Load (path, typeof(GameObject));
		return GameObject.Instantiate (prefab);
	}


	public static GameObject GetCardInstance ()
	{
		GameObject prefab = (GameObject)Resources.Load (CARD_PREFAB_PATH, typeof(GameObject));
		return GameObject.Instantiate (prefab);
	}

	#endregion

	#region INSTANTIATE

	public void PlacePlayerHand (Hand hand)
	{
		PlaceHandObject (hand, true, false);
	}

	public void PlaceDealerHand (Hand hand, bool hasClosedCard)
	{
		PlaceHandObject (hand, false, hasClosedCard);
	}

	private void PlaceHandObject (Hand hand, bool isPlayer, bool hasCloseCard)
	{
		GameObject currentHandObj = GetHandInstance (isPlayer);
		GameObject container = isPlayer ? playerContainer : dealerContainer;
		currentHandObj.transform.SetParent (container.transform, false);
		GameObject obj = null;
		foreach (Card card in hand.cards) {
			obj = GetCardInstance ();
			var fname = card.GetFileName ();
			var sprite = Resources.Load (fname, typeof(Sprite)) as Sprite;
			obj.GetComponent<Image> ().sprite = sprite;
			obj.transform.SetParent (currentHandObj.transform, false);
			obj.transform.SetAsLastSibling ();
		}

		if (hasCloseCard && !isPlayer) {
			DrawBackCard (obj);
		}

		var valueTextObj = currentHandObj.GetComponentInChildren<Text> ();
		valueTextObj.text = hand.GetValue (hasCloseCard).ToString ();
		valueTextObj.transform.parent.SetAsLastSibling ();

		if (isPlayer) {
			playerModel.currentHandObj = currentHandObj;
		} else {
			dealerModel.currentHandObj = currentHandObj;
		}
	}


	private void DrawBackCard (GameObject obj)
	{
		var image = obj.GetComponent<Image> ();
		image.sprite = cardBack;
		dealerModel.SetSecondCardClosed ();
	}

	#endregion
}
