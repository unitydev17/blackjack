﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommonModel {

	public Hand currentHand;
	public GameObject currentHandObj;

	public CommonModel() {
		currentHand = new Hand ();
	}


	public void AddCardToHand(Card card) {
		currentHand.cards.AddFirst (card);
	}


	public virtual void Reset() {
		currentHand = new Hand();
	}
		
	public bool IsBlackJack() {
		return currentHand.IsBlackJack ();
	}

	public bool IsBurst() {
		return currentHand.GetValue () > 21;
	}

	public void SetCurrentHand (Hand hand)
	{
		currentHand.CopyFrom (hand);
	}
}
