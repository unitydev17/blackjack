﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PlayerModel : CommonModel
{

	public List<Hand> hands;
	public UIModel uIModel;
	public int money;
	private bool isAskedForInsurance;


	public PlayerModel() : base ()
	{
		uIModel = new UIModel ();
		hands = new List<Hand> ();
		isAskedForInsurance = true;
	}


	public override void Reset ()
	{
		base.Reset ();
		hands.Clear ();
	}


	public void Win ()
	{
		if (currentHand.IsBlackJack ()) {
			money += currentHand.bet + 3 * currentHand.bet / 2;
			return;
		}
		WinOneToOne ();
	}

	public void WinOneToOne() {
		money += 2 * currentHand.bet;
	}

	public void Share ()
	{
		money += currentHand.bet + currentHand.betInsurance;
	}

	public void AddBet (int num)
	{
		currentHand.bet += num;
		money -= num;
	}

	public void UpdateChips ()
	{
		uIModel.chip100Enable = money >= 100;
		uIModel.chip25Enable = money >= 25;
		uIModel.chip10Enable = money >= 10;
		uIModel.chip5Enable = money >= 5;
		uIModel.chip1Enable = money >= 1;
	}


	public bool IsEnoughMoneyForInsurance() {
		return money >= currentHand.bet / 2;
	}

	public void SetInsurance() {
		currentHand.betInsurance = currentHand.bet / 2;
		money -= currentHand.betInsurance;
		isAskedForInsurance = true;
	}

	public bool IsAskedForInsurance() {
		return isAskedForInsurance;
	}

	public void SetAskedForInsurance(bool value) {
		isAskedForInsurance = value;
	}

	public void PayInsurance() {
		if (IsInsured()) {
			money += currentHand.betInsurance * 2;
			currentHand.betInsurance = 0;
			isAskedForInsurance = false;
		}
	}

	public bool HasBet() {
		return currentHand.bet > 0;
	}

	public bool IsInsured() {
		return currentHand.betInsurance > 0;
	}

	public bool HandMoreThan(CommonModel model) {
		return currentHand.GetValue () > model.currentHand.GetValue (); 
	}

	public bool HandLowerThan(CommonModel model) {
		return currentHand.GetValue () < model.currentHand.GetValue (); 
	}

	public bool HandEquals(CommonModel model) {
		return currentHand.GetValue () == model.currentHand.GetValue (); 
	}

	public void ResetBet() {
		currentHand.bet = 0;
		currentHand.betInsurance = 0;
	}

	public int GetBet() {
		return currentHand.bet;
	}

	public int GetBets() {
		if (hands.Count == 0) {
			return GetBet ();
		}
		return hands.Select (hand => hand.bet).Aggregate ((a, b) => a + b);
	}

	public int GetBetInsurance() {
		return currentHand.betInsurance;
	}

	public bool HasNotBet() {
		return !HasBet ();
	}

	public bool IsSplitableCards() {
		return currentHand.IsSplitableCards ();
	}

	public void SplitHand(List<Card> pair) {
		Hand newHand = currentHand.Split (pair);
		hands.Add (newHand);
		money -= newHand.bet;
	}

	public void AddHand(Hand hand) {
		hand.bet = currentHand.bet;
		hands.Add (hand);
	}

	public void SelectCurrentHand() {
		foreach (Hand hand in hands) {
			if (!hand.isFinalized) {
				currentHand = hand;
				break;
			}
		}
	}

	public bool IsFinalized() {
		int count = 0;
		foreach (Hand hand in hands) {
			if (!hand.isFinalized) {
				count++;
			}
		}
		return count == 1;
	}

	public bool IsEnoughMoneyForSplit() {
		return money >= GetBet ();
	}


	public bool IsAceSplitted() {
		return currentHand.isAceSplitted;
	}

	public void BlockInput() {
		uIModel.blockInput = true;
	}

	public void AllowInput() {
		uIModel.blockInput = false;
	}

	public void ApplyDoubleDown() {
		money -= currentHand.bet;
		currentHand.bet *= 2;
	}

}
