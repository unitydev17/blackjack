﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIModel
{
	public enum DialogState {
		BlackJackWinOneToOne,
		Insurance
	}

	public DialogState dialogState;
	private bool isInit;

	public bool hitActive;
	public bool standActive;
	public bool splitActive;
	public bool doubleDownActive;
	public bool messagePanelActive;
	public bool dialogPanelActive;
	public bool dealActive;
	public bool betHelperActive;
	public bool chip1Enable;
	public bool chip5Enable;
	public bool chip10Enable;
	public bool chip25Enable;
	public bool chip100Enable;
	public bool blockInput;
	public bool chipsAwailable;
	public bool deckSelectorEnable;

	private UIModel prevModel;


	public UIModel ()
	{
		isInit = true;
		Reset ();
	}


	public void Reset ()
	{
		hitActive = false;
		standActive = false;
		splitActive = false;
		doubleDownActive = false;
		dealActive = false;
		messagePanelActive = false;
		dialogPanelActive = false;
		betHelperActive = false;
		chip1Enable = false;
		chip5Enable = false;
		chip10Enable = false;
		chip25Enable = false;
		chip100Enable = false;
		blockInput = false;
		chipsAwailable = false;
		deckSelectorEnable = false;
	}


	public bool HasChanged ()
	{
		bool hasChanged = isInit;
		if (isInit) {
			isInit = false;
		}

		return hasChanged || hitActive != prevModel.hitActive ||
		standActive != prevModel.standActive ||
		splitActive != prevModel.splitActive ||
		doubleDownActive != prevModel.doubleDownActive ||
		messagePanelActive != prevModel.messagePanelActive ||
		dialogPanelActive != prevModel.dialogPanelActive ||
		dealActive != prevModel.dealActive ||
		betHelperActive != prevModel.betHelperActive ||
		chip1Enable != prevModel.chip1Enable ||
		chip5Enable != prevModel.chip5Enable ||
		chip10Enable != prevModel.chip10Enable ||
		chip25Enable != prevModel.chip25Enable ||
		chip100Enable != prevModel.chip100Enable ||
		blockInput != prevModel.blockInput ||
		chipsAwailable != prevModel.chipsAwailable ||
		deckSelectorEnable != prevModel.deckSelectorEnable;
	}


	public void Refresh ()
	{
		Copy (this, ref prevModel);
	}


	private void Copy (UIModel modelFrom, ref UIModel modelTo)
	{
		if (modelTo == null) {
			modelTo = new UIModel ();
		}

		modelTo.hitActive = modelFrom.hitActive;
		modelTo.standActive = modelFrom.standActive;
		modelTo.splitActive = modelFrom.splitActive;
		modelTo.doubleDownActive = modelFrom.doubleDownActive;
		modelTo.messagePanelActive = modelFrom.messagePanelActive;
		modelTo.dialogPanelActive = modelFrom.dialogPanelActive;
		modelTo.dealActive = modelFrom.dealActive;
		modelTo.betHelperActive = modelFrom.betHelperActive;
		modelTo.chip1Enable = modelFrom.chip1Enable;
		modelTo.chip5Enable = modelFrom.chip5Enable;
		modelTo.chip10Enable = modelFrom.chip10Enable;
		modelTo.chip25Enable = modelFrom.chip25Enable;
		modelTo.chip100Enable = modelFrom.chip100Enable;
		modelTo.blockInput = modelFrom.blockInput;
		modelTo.chipsAwailable = modelFrom.chipsAwailable;
		modelTo.deckSelectorEnable = modelFrom.deckSelectorEnable;
	}

}
