﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealerModel : CommonModel {

	private bool isSecondCardClosed;

	public void SetSecondCardClosed() {
		SetSecondCardClosed (true);
	}

	public void UnsetSecondCardClosed() {
		SetSecondCardClosed (false);
	}

	private void SetSecondCardClosed(bool value) {
		isSecondCardClosed = value;
	}

	public bool IsSecondCardClosed() {
		return isSecondCardClosed;
	}

	public bool IsBlackJackPossible() {
		return currentHand.IsBlackJackPossible ();
	}

	public bool IsAceOpened() {
		return currentHand.IsAceOpened () && isSecondCardClosed;
	}
}
