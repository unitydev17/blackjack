﻿public enum State {
	OnSetupBet,
	OnStartGame,
	Idle,
	PlayerMove,
	DealerMove,
	CheckWinner

}
